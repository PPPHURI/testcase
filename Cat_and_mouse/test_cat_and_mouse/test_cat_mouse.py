from cat_and_mouse.cat_mouse import catAndMouse
import unittest
class PrimeListTest(unittest.TestCase):
    def test_give_xyz_is_input_1(self):
        
        x_input = 1
        y_input = 2
        z_input = 3
        expect_op="Cat B"
        self.assertEqual(catAndMouse(x_input,y_input,z_input),expect_op)

    def test_give_xyz_is_input_2(self):

        
        x_input = 1
        y_input = 3
        z_input = 2
        expect_op="Mouse C"
        self.assertEqual(catAndMouse(x_input,y_input,z_input),expect_op)

    def test_give_xyz_is_input_3(self):
        x_input = 40
        y_input = 50
        z_input = 30
        expect_op = "Cat A"


        self.assertEqual(catAndMouse(x_input,y_input,z_input),expect_op)
