from  Funnystring.funnystring  import funnystring

import unittest


class PrimeListTest(unittest.TestCase):
    def test_give_acxz_is_input_test_funnystring_(self):

        a_input='acxz'
        expect_op="Funny"

        self.assertEqual(funnystring(a_input),expect_op)

    def test_give_bcxz_is_input_test_funnystring_1(self):
        
        b_input='bcxz'
        expect_op="Not Funny"

        self.assertEqual(funnystring(b_input),expect_op)