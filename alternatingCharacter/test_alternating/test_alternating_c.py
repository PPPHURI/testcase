from  alternating.alternating  import alternatingCharacters

import unittest


class PrimeListTest(unittest.TestCase):
    def test_give_AAAA_is_input_test_alternating_(self):

        num_ip="AAAA"
        expect_op=3

        self.assertEqual(alternatingCharacters(num_ip),expect_op)
    
    # def test_give_BBBBB_is_input_test_alternating_(self):

    #     num_ip="BBBBB"
    #     expect_op=4

    #     self.assertEqual(alternatingCharacters(num_ip),expect_op)
    
    def test_give_ABABABAB_is_input_test_alternating_1(self):

        num_ip="ABABABAB"
        expect_op=0

        self.assertEqual(alternatingCharacters(num_ip),expect_op)
    
    def test_give_AAABBB_is_input_test_alternating_2(self):

        num_ip="AAABBB"
        expect_op=4

        self.assertEqual(alternatingCharacters(num_ip),expect_op)