from character.alternate import alternate

import unittest

class PrimeListTest(unittest.TestCase):
    def test_give_beabeefeab_is_input_test_(self):

        s_input="beabeefeab"
        expect_op=5

        self.assertEqual(alternate(s_input),expect_op)

    