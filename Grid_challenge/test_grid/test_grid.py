from grid_challenge.grid import gridChallenge
import unittest

class PrimeListTest(unittest.TestCase):
    def test_give_(self):

        a_input=['ebacd', 'fghij', 'olmkn', 'trpqs', 'xywuv']
        expect_op="YES"

        self.assertEqual(gridChallenge(a_input),expect_op)

    def test_give_2(self):

        a_input=['lyvir', 'jgfew', 'uweor', 'qxwyr', 'uikjd']
        expect_op="NO"

        self.assertEqual(gridChallenge(a_input),expect_op)

        