def fizzbuzz(d):   
    if d % 3 == 0 and d % 5 == 0:
        return("fizzbuzz")
        
    elif d % 3 == 0:
        return("fizz")
        
    elif d % 5 == 0:
        return("buzz")
        
    else:
        return("no fizzbuzz")