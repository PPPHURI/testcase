from  Fizzbuzz.Fizzbuzz  import fizzbuzz

import unittest


class PrimeListTest(unittest.TestCase):
    def test_give_45_is_input_test_fizzbuzz_(self):

        num_ip=45
        expect_op="Fizzbuzz"

        self.assertEqual(fizzbuzz(num_ip),expect_op)

    def test_give_45_is_input_test_fizzbuzz_2(self):

        num_ip=3
        expect_op="Fizz"

        self.assertEqual(fizzbuzz(num_ip),expect_op)

    def test_give_45_is_input_test_fizzbuzz_3(self):

        num_ip=5
        expect_op="buzz"

        self.assertEqual(fizzbuzz(num_ip),expect_op)
    
    def test_give_45_is_input_test_fizzbuzz_4(self):

        num_ip=7
        expect_op="no fizzbuzz"

        self.assertEqual(fizzbuzz(num_ip),expect_op)
