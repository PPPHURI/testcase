def caesarCipher(s, j):
    a="abcdefghijklmnopqrstuvwxyz"
    A="ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    APB=[i for i in A]
    apb=[i for i in a]
    word=""
    for i in s:
        if i in apb:
            idx=apb.index(i)
            if (idx+j) <26:
                word+=apb[idx+j]
            else:
                m=(idx+j)-26
                while m>25:
                    m=m-26
                word+=apb[m]
        elif i in APB:
            idx=APB.index(i)
            if (idx+j) <26:
                word+=APB[idx+j]
            else:
                m=(idx+j)-26
                while m>25:
                    m=m-26
                word+=APB[m]
        else:
            word+=i
    return word  