from cipher.cipher import caesarCipher
import unittest

class PrimeListTest(unittest.TestCase):
    def test_give_str_input_1(self):

        a_input="Hello_World!"
        num_input=4
        expect_op="Lipps_Asvph!"

        self.assertEqual(caesarCipher(a_input,num_input),expect_op)
    
    def test_give_str_input_2(self):

        a_input="Always-Look-on-the-Bright-Side-of-Life"
        num_input=5
        expect_op="Fqbfdx-Qttp-ts-ymj-Gwnlmy-Xnij-tk-Qnkj"

        self.assertEqual(caesarCipher(a_input,num_input),expect_op)

    def test_give_str_input_3(self):

        a_input="1X7T4VrCs23k4vv08D6yQ3S19G4rVP188M9ahuxB6j1tMGZs1m10ey7eUj62WV2exLT4C83zl7Q80M"
        num_input=27
        expect_op="1Y7U4WsDt23l4ww08E6zR3T19H4sWQ188N9bivyC6k1uNHAt1n10fz7fVk62XW2fyMU4D83am7R80N"

        self.assertEqual(caesarCipher(a_input,num_input),expect_op)